
public class TemperaturtabelleJava {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//deklaration und initialisierung
		double c1, c2, c3, c4, c5, f1, f2, f3, f4, f5;
		
		f1 = -20;
		f2 = -10;
		f3 = 0;
		f4 = 20;
		f5 = 30;
		
		c1 = (f1 -32) / 1.8;
		c2 = (f2 -32) / 1.8;
		c3 = (f3 -32) / 1.8;
		c4 = (f4 -32) / 1.8;
		c5 = (f5 -32) / 1.8;
		
		System.out.printf("%-12s" , "Fahrenheit");
		System.out.printf("|");
		System.out.printf("%10s", "Celsius");
		System.out.println();
		System.out.println("------------------------");
		
		System.out.printf("%-12s", f1);
		System.out.printf("|");
		System.out.printf("%10.6s", c1);
		System.out.println();
		
		System.out.printf("%-12s", f2);
		System.out.printf("|");
		System.out.printf("%10.6s", c2);
		System.out.println();
		
		System.out.printf("%-12s","+"+f3);
		System.out.printf("|");
		System.out.printf("%10.6s", c3);
		System.out.println();
		
		System.out.printf("%-12s","+"+ f4);
		System.out.printf("|");
		System.out.printf("%10.5s", c4);
		System.out.println();
		
		System.out.printf("%-12s","+"+ f5);
		System.out.printf("|");
		System.out.printf("%10.5s", c5);
	}

}
